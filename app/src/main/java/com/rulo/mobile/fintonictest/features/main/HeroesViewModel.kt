package com.rulo.mobile.fintonictest.features.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rulo.mobile.fintonictest.repository.ApiClient
import com.rulo.mobile.fintonictest.repository.database.FintonicTestDB
import com.rulo.mobile.fintonictest.repository.model.Hero
import com.rulo.mobile.fintonictest.repository.remote.heroes.HeroesResponse
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber

class HeroesViewModel(app: Application) : AndroidViewModel(app) {
    private val database = FintonicTestDB.getInstance(app)
    var heroesList: LiveData<List<Hero>> = database.heroeDao().all
    var selectedHero: MutableLiveData<Hero?> = MutableLiveData(null)

    fun setSelectedHero(hero: Hero?) {
        selectedHero.postValue(hero)
    }

    fun getRemoteHeroes() {
        ApiClient.service.getHeroes().enqueue(object : retrofit2.Callback<HeroesResponse> {
            override fun onFailure(call: Call<HeroesResponse>, t: Throwable) {
                t.printStackTrace()
                Timber.wtf(t)
            }

            override fun onResponse(call: Call<HeroesResponse>, response: Response<HeroesResponse>) {
                if (response.isSuccessful) {
                    Timber.wtf(response.body().toString())
                    if (response.body() != null) {
                        val heroesList: ArrayList<Hero> = response.body()!!.superheroes
                        heroesList.map {
                            it.uid = it.hashCode().toString()
                            GlobalScope.async {
                                database.heroeDao().insertAll(it)
                            }
                        }
                    }

                }
            }
        })
    }

}