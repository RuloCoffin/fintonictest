package com.rulo.mobile.fintonictest.features.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import com.rulo.mobile.fintonictest.R
import com.rulo.mobile.fintonictest.repository.model.Hero
import com.rulo.mobile.fintonictest.utils.extensions.setImageCenterCrop
import kotlinx.android.synthetic.main.hero_item.view.*

class HeroesAdapter : RecyclerView.Adapter<HeroesAdapter.HeroeViewHolder>() {

    private val heroList =
        SortedList<Hero>(Hero::class.java, object : SortedListAdapterCallback<Hero>(this) {
            override fun areItemsTheSame(item1: Hero?, item2: Hero?): Boolean {
                return item1?.uid.equals(item2?.uid)
            }

            override fun compare(o1: Hero, o2: Hero): Int {
                return o1.name.compareTo(o2.name)
            }

            override fun areContentsTheSame(oldItem: Hero?, newItem: Hero?): Boolean {
                return oldItem?.toString() == newItem?.toString()
            }
        })

    private var iHeroClickedListener: IHeroClickedListener? = null

    fun setHeroListener(heroListener: IHeroClickedListener): HeroesAdapter {
        this.iHeroClickedListener = heroListener
        return this
    }

    fun addHeroes(heroes: List<Hero>) {
        heroList.addAll(heroes)
    }


    override fun getItemCount(): Int {
        return heroList.size()
    }

    override fun onBindViewHolder(holder: HeroeViewHolder, position: Int) {
        holder.bind(heroList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroeViewHolder {
        return HeroeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.hero_item, parent, false))
    }

    inner class HeroeViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val txtHeroName = view.txt_hero_name
        private val imgHero = view.img_hero_picture
        private val viewHeroContainer = view.view_hero_container
        fun bind(hero: Hero) {
            txtHeroName.text = hero.name
            viewHeroContainer.setOnClickListener {
                if (iHeroClickedListener != null)
                    iHeroClickedListener!!.onHeroClicked(hero)
            }
            imgHero.setImageCenterCrop(hero.photo)
        }
    }
}

interface IHeroClickedListener {
    fun onHeroClicked(hero: Hero)
}