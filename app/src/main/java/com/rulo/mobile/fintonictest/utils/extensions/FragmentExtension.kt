package com.rulo.mobile.fintonictest.utils.extensions

import android.view.View
import androidx.annotation.AnyRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.rulo.mobile.fintonictest.R

/**
 * Launch fragment from FragmentManager(v4)
 * @param[fragment] Instance of fragment to show.
 * @param[idContainer] Name id of container where will it show.
 * @param[tag] String for set, it is optional.
 * @param[sharedElements] HashMap of widgets, this is used for create animations
 * @param[hasOptionMenu] If true, the fragment has menu items to contribute.
 * @param[STACKMODE] Type of stack mode, this will serve for manage the back stack.
 */

fun FragmentManager.loadFragment(
    fragment: Fragment,
    @AnyRes idContainer: Int,
    tag: String = fragment::class.java.simpleName,
    sharedElements: HashMap<String, View>? = null,
    hasOptionMenu: Boolean = false,
    animationType: AnimationType = AnimationType.NONE,
    STACKMODE: StackMode = StackMode.INCLUSIVE
) {
    fragment.setHasOptionsMenu(hasOptionMenu)
    val fragmentTransaction = this.beginTransaction()

    when (animationType) {
        AnimationType.ZOOM -> fragmentTransaction.setCustomAnimations(
            R.anim.zoom_in,
            R.anim.zoom_out,
            R.anim.zoom_in,
            R.anim.zoom_out
        )
        AnimationType.FADE -> fragmentTransaction.setCustomAnimations(
            android.R.anim.fade_in,
            android.R.anim.fade_out,
            android.R.anim.fade_in,
            android.R.anim.fade_out
        )
        AnimationType.SLIDE -> fragmentTransaction.setCustomAnimations(
            android.R.anim.slide_in_left,
            android.R.anim.slide_out_right,
            android.R.anim.slide_in_left,
            android.R.anim.slide_out_right
        )
    }

    when (STACKMODE) {
        StackMode.INCLUSIVE -> {
            this.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            fragmentTransaction.replace(idContainer, fragment, tag)
        }
        StackMode.REPLACE_STACK -> {
            if (this.fragmentAlreadyExist(tag) && getTopFragment(this) == this.findFragmentByTag(tag)) return
            fragmentTransaction.replace(idContainer, fragment, tag)
            fragmentTransaction.addToBackStack(tag)
            fragmentTransaction.show(fragment)
        }
        StackMode.ADD_STACK -> {
            if (this.fragmentAlreadyExist(tag) || fragment.isAdded) return
            fragmentTransaction.add(idContainer, fragment, tag)
            fragmentTransaction.addToBackStack(tag)
            getTopFragment(this)?.let { fragmentTransaction.hide(it) }
            fragmentTransaction.show(fragment)
        }
        StackMode.REPLACE -> fragmentTransaction.replace(idContainer, fragment, tag)
    }

    if (sharedElements != null && !sharedElements.isEmpty()) {
        for (element in sharedElements.entries) {
            fragmentTransaction.addSharedElement(element.value, element.key)
        }
    }
    fragmentTransaction.commit()
}


fun FragmentManager.fragmentAlreadyExist(tag: String): Boolean {
    return this.findFragmentByTag(tag) != null
}

private fun getTopFragment(fragmentManager: FragmentManager): Fragment? {
    fragmentManager.run {
        return when (backStackEntryCount) {
            0 -> null
            else -> findFragmentByTag(getBackStackEntryAt(backStackEntryCount - 1).name)
        }
    }
}


/**
 * This list indicate the manage of BackStack
 * INCLUSIVE -> Clean back stack and set fragment.
 * REPLACE_STACK -> Replace current fragment and add this to back stack.
 * ADD_STACK -> Add current fragment and add this to back stack.
 * REPLACE -> Only replace old by new fragment
 */
enum class StackMode {
    INCLUSIVE, REPLACE_STACK, ADD_STACK, REPLACE
}

enum class AnimationType {
    NONE, ZOOM, FADE, SLIDE
}