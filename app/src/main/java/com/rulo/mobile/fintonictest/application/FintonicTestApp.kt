package com.rulo.mobile.fintonictest.application

import android.app.Application
import com.rulo.mobile.fintonictest.BuildConfig
import timber.log.Timber

class FintonicTestApp : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            // ToDo Change to crashlitics or another
            Timber.plant(Timber.DebugTree())
        }
    }
}