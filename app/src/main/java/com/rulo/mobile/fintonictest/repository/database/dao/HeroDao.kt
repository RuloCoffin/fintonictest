package com.rulo.mobile.fintonictest.repository.database.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.rulo.mobile.fintonictest.repository.model.Hero


@Dao
interface HeroDao {
    @get:Query("SELECT * FROM heroes")
    val all: LiveData<List<Hero>>

    @Query("SELECT COUNT(*) from heroes")
    fun countUsers(): Int

    @Insert
    fun insertAll(vararg heroes: Hero)

    @Delete
    fun delete(user: Hero)
}