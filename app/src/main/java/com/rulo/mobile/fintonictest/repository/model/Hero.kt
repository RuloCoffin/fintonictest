package com.rulo.mobile.fintonictest.repository.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "heroes")
data class Hero(
    @PrimaryKey
    var uid: String = "",
    @SerializedName("abilities")
    val abilities: String? = null,
    @SerializedName("groups")
    val groups: String? = null,
    @SerializedName("height")
    val height: String? = null,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("photo")
    val photo: String? = null,
    @SerializedName("power")
    val power: String? = null,
    @SerializedName("realName")
    val realName: String = ""
) : Parcelable {
    override fun toString(): String {
        return "Hero(uid=$uid, abilities=$abilities, groups=$groups, height=$height, name='$name', photo=$photo, power=$power, realName='$realName')"
    }
}