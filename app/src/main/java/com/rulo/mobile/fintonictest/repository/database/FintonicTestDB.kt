package com.rulo.mobile.fintonictest.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rulo.mobile.fintonictest.repository.database.dao.HeroDao
import com.rulo.mobile.fintonictest.repository.model.Hero


@Database(entities = [Hero::class], version = 1)
abstract class FintonicTestDB : RoomDatabase() {
    abstract fun heroeDao(): HeroDao

    companion object {
        private var INSTANCE: FintonicTestDB? = null

        fun getInstance(context: Context): FintonicTestDB {
            if (INSTANCE == null) {
                synchronized(FintonicTestDB::class) {
                    INSTANCE =
                        Room.databaseBuilder(
                            context.applicationContext,
                            FintonicTestDB::class.java,
                            this::class.java.simpleName
                        ).build()
                }
            }
            return INSTANCE!!

            fun destroyInstance() {
                INSTANCE = null
            }
        }
    }
}