package com.rulo.mobile.fintonictest.repository.remote.heroes

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.rulo.mobile.fintonictest.repository.model.Hero

class HeroesResponse {
    @SerializedName("superheroes")
    @Expose
    val superheroes: ArrayList<Hero> = ArrayList()

    override fun toString(): String {
        return "HeroesResponse(superheroes=$superheroes)"
    }


}