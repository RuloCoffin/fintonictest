package com.rulo.mobile.fintonictest.features.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    var appState: MutableLiveData<MainStates> = MutableLiveData(MainStates.STATE_HEROES_LIST)


    fun setStateList() {
        appState.postValue(MainStates.STATE_HEROES_LIST)
    }

    fun setStateDetail() {
        appState.postValue(MainStates.STATE_HERO_DETAIL)
    }

    enum class MainStates {
        STATE_HEROES_LIST,
        STATE_HERO_DETAIL
    }

}