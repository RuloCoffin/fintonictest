package com.rulo.mobile.fintonictest.repository.remote

import com.rulo.mobile.fintonictest.repository.remote.heroes.HeroesResponse
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("bins/bvyob")
    fun getHeroes(): Call<HeroesResponse>


}