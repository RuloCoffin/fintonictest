package com.rulo.mobile.fintonictest.features.main.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.chip.Chip
import com.rulo.mobile.fintonictest.R
import com.rulo.mobile.fintonictest.features.main.HeroesViewModel
import com.rulo.mobile.fintonictest.repository.model.Hero
import com.rulo.mobile.fintonictest.utils.extensions.setImageCenterCropRound
import kotlinx.android.synthetic.main.fragment_hero_detail.*

private const val ARH_HERO = "arg_hero"

class HeroDetailFragment : Fragment() {
    private var currentHero: Hero? = null
    private lateinit var heroesViewModel: HeroesViewModel
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_hero_detail, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        heroesViewModel = activity?.run {
            ViewModelProviders.of(this).get(HeroesViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        arguments?.let {
            currentHero = it.getParcelable(ARH_HERO)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (currentHero != null)
            with(currentHero!!) {
                img_hero_detail.setImageCenterCropRound(this.photo)
                txt_hero_name_detail.text = this.realName
                txt_hero_height_detail.text = this.height
                txt_hero_powers_detail.text = this.power
                txt_hero_abilities_detail.text = this.abilities
                with(this.groups) {
                    chip_group_members.removeAllViews()
                    if (!this.isNullOrBlank())
                        this.split(",").sortedBy { x -> x.length }.map {
                            chip_group_members.addView(Chip(chip_group_members.context).apply {
                                text = it
                            })
                        }
                }
            }
    }

    companion object {
        @JvmStatic
        fun newInstance(hero: Hero) =
                HeroDetailFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARH_HERO, hero)
                    }
                }
    }
}
