package com.rulo.mobile.fintonictest.features.main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.rulo.mobile.fintonictest.R
import com.rulo.mobile.fintonictest.features.main.fragment.HeroDetailFragment
import com.rulo.mobile.fintonictest.features.main.fragment.HeroListFragment
import com.rulo.mobile.fintonictest.utils.extensions.AnimationType
import com.rulo.mobile.fintonictest.utils.extensions.StackMode
import com.rulo.mobile.fintonictest.utils.extensions.loadFragment

class MainActivity : AppCompatActivity() {
    private val heroesViewModel by lazy {
        ViewModelProviders.of(this).get(HeroesViewModel::class.java)
    }

    private val mainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        heroesViewModel.getRemoteHeroes()
        setEvents()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (supportFragmentManager.backStackEntryCount == 0) {
            heroesViewModel.setSelectedHero(null)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setEvents() {
        mainViewModel.appState.observe(this, Observer {
            val heroListFragment = HeroListFragment.newInstance()
            when (it) {
                MainViewModel.MainStates.STATE_HEROES_LIST -> {
                    supportActionBar?.setDisplayHomeAsUpEnabled(false)
                    if (!heroListFragment.isAdded)
                        supportFragmentManager.loadFragment(
                            heroListFragment,
                            R.id.container_main,
                            STACKMODE = StackMode.REPLACE
                        )
                }
                MainViewModel.MainStates.STATE_HERO_DETAIL -> {
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    supportFragmentManager.loadFragment(
                        HeroDetailFragment.newInstance(heroesViewModel.selectedHero.value!!),
                        R.id.container_main,
                        animationType = AnimationType.SLIDE,
                        STACKMODE = StackMode.ADD_STACK
                    )
                }
            }
        })

        heroesViewModel.selectedHero.observe(this, Observer {
            if (it != null) {
                supportActionBar?.title = it.name
                mainViewModel.setStateDetail()
            } else {
                supportActionBar?.title = getString(R.string.app_name)
                mainViewModel.setStateList()
            }
        })
    }
}
