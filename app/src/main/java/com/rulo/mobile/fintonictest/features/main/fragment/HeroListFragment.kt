package com.rulo.mobile.fintonictest.features.main.fragment


import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.rulo.mobile.fintonictest.R
import com.rulo.mobile.fintonictest.features.main.HeroesViewModel
import com.rulo.mobile.fintonictest.features.main.adapter.HeroesAdapter
import com.rulo.mobile.fintonictest.features.main.adapter.IHeroClickedListener
import com.rulo.mobile.fintonictest.repository.model.Hero
import kotlinx.android.synthetic.main.fragment_hero_list.*
import timber.log.Timber


class HeroListFragment : Fragment(), IHeroClickedListener {
    private lateinit var heroesViewModel: HeroesViewModel
    private val heroesAdapter: HeroesAdapter = HeroesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        heroesAdapter.setHeroListener(this)
        heroesViewModel = activity?.run {
            ViewModelProviders.of(this).get(HeroesViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_heroes.apply {
            layoutManager = GridLayoutManager(
                context,
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 3 else 2
            )
            adapter = heroesAdapter
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setEvents()
    }

    private fun setEvents() {
        heroesViewModel.heroesList.observe(this, Observer {
            heroesAdapter.addHeroes(it)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hero_list, container, false)
    }


    override fun onHeroClicked(hero: Hero) {
        Timber.wtf(hero.toString())
        heroesViewModel.setSelectedHero(hero)
    }


    companion object {
        @JvmStatic
        fun newInstance() =
            HeroListFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

}
