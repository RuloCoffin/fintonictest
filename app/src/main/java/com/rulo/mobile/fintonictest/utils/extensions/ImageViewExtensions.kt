package com.rulo.mobile.fintonictest.utils.extensions

import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.rulo.mobile.fintonictest.R

fun ImageView.setImageCenterCrop(uri: String?) {
    Glide.with(this.context).load(uri).placeholder(
        CircularProgressDrawable(this.context).apply {
            strokeWidth = 4f
            centerRadius = 25f
        }.also {
            it.start()
        }
    )
        .centerCrop()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .error(R.drawable.ic_error).into(this)
}

fun ImageView.setImageCenterCropRound(uri: String?) {
    Glide.with(this.context).load(uri).placeholder(
        CircularProgressDrawable(this.context).apply {
            strokeWidth = 4f
            centerRadius = 25f
        }.also {
            it.start()
        }
    )
        .centerCrop()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .apply(RequestOptions.circleCropTransform())
        .error(R.drawable.ic_error).into(this)
}